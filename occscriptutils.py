import sys
sys.path.append("..")
from jsonserializable import JSONSerializable
import numpy as np

class Rectangle:
    def __init__(self, topLeftCornerPoint, width, height):
        self.topLeftCornerPoint = topLeftCornerPoint
        self.width = width
        self.height = height

    def setTopLeftCornerPoint(self, topLeftCornerPoint):
        self.topLeftCornerPoint = topLeftCornerPoint

class Circle(JSONSerializable):
    def __init__(self, centroid=[0,0], radius=1):
        self.centroid = np.array(centroid)
        self.radius = radius
    def setCentroid(self, centroid):
        self.centroid = centroid
    def setRadius(self,radius):
        self.radius = radius
    def incrementRadius(self, increment, isIncrement):
        if isIncrement:
            self.radius=self.radius+increment
        else:
            self.radius=self.radius-increment
            if(self.radius<=1):
                self.radius = 1

    def moveY(self,increment,isUp):
        if isUp:
            self.centroid[1] = self.centroid[1]-increment
        else:
            self.centroid[1] = self.centroid[1]+increment

    def moveX(self,increment,isRight):
        if isRight:
            self.centroid[0] = self.centroid[0]+increment
        else:
            self.centroid[0] = self.centroid[0]-increment

    def toJSON(self):
        data = {"circle": {
            "center": [int(self.centroid[0]),int(self.centroid[1])],
            "radius": self.radius
        }}
        return data

    def initFromJSON(self,jsonData):
        self.centroid = jsonData["circle"]["center"]
        self.radius = jsonData["circle"]["radius"]

def isRectangleWithinCircle(rectangle, circle):
    dx = max(abs(circle.centroid[0] - rectangle.topLeftCornerPoint[0]),
             abs(rectangle.topLeftCornerPoint[0] + rectangle.width - circle.centroid[0]))
    dy = max(abs(circle.centroid[1] - rectangle.topLeftCornerPoint[1]),
             abs(rectangle.topLeftCornerPoint[1] + rectangle.height - circle.centroid[1]))
    return (circle.radius * circle.radius) >= (dx * dx) + (dy * dy)