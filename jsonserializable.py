import json

class JSONSerializable:
    def toJSON(self):
        raise NotImplementedError( "Should have implemented this" )
    def initFromJSON(self,jsonData):
        raise NotImplementedError( "Should have implemented this" )