from threading import Thread, Lock
import cv2
import numpy as np
import collections
import time
import os
import matplotlib.pyplot as plt
from occrx.occParams import OCCRxParams

class FrameStreamer:

    def __init__(self, framestreamer, isRealTime, cameraParameters):

        self._framestreamer = framestreamer;
        self._streamer = cv2.VideoCapture(framestreamer)
        for i in range(3):
            if not self._streamer.isOpened():
                print("The frame streamer is not available. Trying again ({})".format(i))
                self._streamer = cv2.VideoCapture(framestreamer)
                key = cv2.waitKey(1000)

        self._realTime = isRealTime

        if(isRealTime):
            resolution = cameraParameters.resolution
            # frameWidth = int(resolution[0])
            # frameHeight = int(resolution[1])
            # fps = cameraParameters.samplingFrequency
            # gain = cameraParameters.gain
            # wbtm = 0
            # wbt = cameraParameters.whitebalance
            # expm = 1
            # exp = cameraParameters.exposure

            fourcc = cv2.VideoWriter_fourcc(*'MJPG')
            self._streamer.set(cv2.CAP_PROP_FOURCC, fourcc)
            self._streamer.set(cv2.CAP_PROP_FRAME_WIDTH, cameraParameters.resolution[0])
            self._streamer.set(cv2.CAP_PROP_FRAME_HEIGHT, cameraParameters.resolution[1])
            bSuccess = self._streamer.set(cv2.CAP_PROP_FPS, cameraParameters.samplingFrequency)
            self._streamer.set(cv2.CAP_PROP_AUTO_EXPOSURE,0.25)
            time.sleep(1)
            self._streamer.set(cv2.CAP_PROP_EXPOSURE,cameraParameters.exposure)
            time.sleep(1)
            self._streamer.set(cv2.CAP_PROP_GAIN,cameraParameters.gain)

            print("Camera parameters")
            print("Resolution: {}x{}".format(self._streamer.get(cv2.CAP_PROP_FRAME_WIDTH),self._streamer.get(cv2.CAP_PROP_FRAME_HEIGHT)))
            print("Fps: {}".format(self._streamer.get(cv2.CAP_PROP_FPS)))
            print("Exposure: {}".format(self._streamer.get(cv2.CAP_PROP_EXPOSURE)))
            print("Gain: {}".format(self._streamer.get(cv2.CAP_PROP_GAIN)))

        self._isFrameGrabbed, self._frame = self._streamer.read()
        # cv2.imshow("frame", self._frame)
        # cv2.waitKey(1)
        # Initialize flags and thread variables
        self._isCapturing = False
        self._isFrameAvailable = True
        self._thread = None
        # self._readLock = Lock()

        # Buffer
        self._input_deque_count = 0;
        self._input_deque = collections.deque(maxlen=50)

    def start(self):
        self._isCapturing = True
        if self._thread == None:
            self._thread = Thread(target=self.update, args=())
            self._thread.daemon = True
            self._thread.start()
            print('Camera has started recording')
            return self

    def update(self):
        while (self._isCapturing):
            if(self._realTime):
                success, frame = self._streamer.read()
                print(success)
                if(success):
                    self._input_deque.append(frame)
                    self._input_deque_count = self._input_deque_count + 1
                    print(self._input_deque_count)
                else:
                    print(success)
            else:
                if(not self._isFrameAvailable):
                    success, frame = self._streamer.read()
                    self._readLock.acquire()
                    self._isFrameGrabbed, self._frame = success, frame
                    self._isFrameAvailable = True
                    self._readLock.release()
            if(not self._isCapturing):
                print("The camera is going to stop")
                break
        self._streamer.release()
        self._streamer.close()
        self._streamer = None
        return

    def isCapturing(self):
        return self._isCapturing

    def isFrameAvailable(self):
        return self._isFrameAvailable

    def grabFrame(self):
        frame = None
        if(self._realTime):
            if(self._input_deque_count>0):
                print("Acquired Frame")
                frame = self._input_deque.popleft().copy()
                self._input_deque_count = self._input_deque_count -1
            else:
                # print("There is no elements within the queue")
                return False, None
            return True, frame
        else:
            if(self._isFrameAvailable):
                self._readLock.acquire()
                success, frame = self._isFrameGrabbed, self._frame
                self._isFrameAvailable=False
                self._readLock.release()
                if(success):
                    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                    frame = frame.astype(np.float32) / 255.0
                    # Gamma correction
                    frame = frame ** (1.8)
                    return success, frame
                else:
                    return False, None

    def setFrameStart(self,number):
        totalFrames = self._streamer.get(cv2.CAP_PROP_FRAME_COUNT)
        if(number >= 0 & number <= totalFrames):
            self._streamer.set(cv2.CAP_PROP_POS_FRAMES,number)
            self._isFrameGrabbed, self._frame = self._streamer.read()

    def stop(self):
        self._isCapturing = False
        if self._thread.is_alive():
            self._thread.join()
            self._thread == None
            print("The camera has been stopped")

    def flush(self):
        self._input_deque_count = 0
        self._input_deque.clear()

def adaptFrame(frame):
    # frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    # frame = frame.astype(np.float32) / 255.0
    # Gamma correction
    # frameInput = frameInput ** (1.8)
    return frame