import numpy as np
import time

def binarize(signal, threshold, roi = []):
    if(len(roi)==0):
        numberOfSamples = len(signal)

        redChannel = np.zeros(numberOfSamples).reshape(numberOfSamples, 1)
        greenChannel = np.zeros(numberOfSamples).reshape(numberOfSamples, 1)
        blueChannel = np.zeros(numberOfSamples).reshape(numberOfSamples, 1)

        redChannel[np.less(threshold[:, 0] / 2, signal[:, 0], where=True)] = 1.0
        greenChannel[np.less(threshold[:, 1] / 2, signal[:, 1], where=True)] = 1.0
        blueChannel[np.less(threshold[:, 2] / 2, signal[:, 2], where=True)] = 1.0

        binarizeSignal = np.concatenate((redChannel, greenChannel, blueChannel), axis=1)
    else:
        start = roi[0][1]
        stop = roi[1][1]
        numberOfSamples = len(signal)

        redChannel = np.zeros(numberOfSamples).reshape(numberOfSamples, 1)
        greenChannel = np.zeros(numberOfSamples).reshape(numberOfSamples, 1)
        blueChannel = np.zeros(numberOfSamples).reshape(numberOfSamples, 1)

        redChannel[np.less(threshold[start:stop, 0] / 2, signal[:, 0], where=True)] = 1.0
        greenChannel[np.less(threshold[start:stop, 1] / 2, signal[:, 1], where=True)] = 1.0
        blueChannel[np.less(threshold[start:stop, 2] / 2, signal[:, 2], where=True)] = 1.0

        binarizeSignal = np.concatenate((redChannel, greenChannel, blueChannel), axis=1)

    return binarizeSignal

def convertByteToChar(bits, encoding='utf-8', errors='surrogatepass'):
    bits = ''.join(str(i) for i in bits)
    n = int(bits,2)
    return n

def decode(signal,syncTemplate1D,threshold=0.8, samplePoints = []):
    tic = time.clock()
    redChannel = signal[:, 0]
    # greenChannel = signal[:, 1]
    blueChannel = signal[:, 2]
    #
    # syncTemplateN = (syncTemplate1D - np.mean(syncTemplate1D)) / (np.std(syncTemplate1D) * len(syncTemplate1D))
    # greenChannelN = (greenChannel - np.mean(greenChannel)) / np.std(greenChannel)
    # greenChannelN = np.reshape(greenChannelN, len(greenChannelN))
    # correlation = np.correlate(greenChannelN, syncTemplateN)
    #
    # maxValue = np.amax(correlation)
    # if(maxValue >= threshold):
    #     maxIndexValue = np.argmax(correlation)

        # numberOfSamples = syncTemplate1D.shape[0] // 5
    numberOfSamples = signal.shape[0] // 5
    index = np.arange(numberOfSamples, syncTemplate1D.shape[0] + 1, numberOfSamples)
    index = index - numberOfSamples // 2
    index = index
    for idx in index:
        samplePoints.append(idx)
    redChannelValues = np.take(redChannel, indices=index)
    blueChannelValues = np.take(blueChannel, indices=index)

    redData = redChannelValues[0:]
    blueData = blueChannelValues[0:]
    data = np.array(list(zip(blueData, redData)))
    toc = time.clock()
    return True, data

def checkCRC(data, key):
    remainder = decodeData(data, key)
    return ('1' not in remainder)

def xor(a, b):
    # initialize result
    result = []

    # Traverse all bits, if bits are
    # same, then XOR is 0, else 1
    for i in range(1, len(b)):
        if a[i] == b[i]:
            result.append('0')
        else:
            result.append('1')

    return ''.join(result)

def decodeData(data, key):
    l_key = len(key)

    # Appends n-1 zeroes at end of data
    appended_data = data + '0' * (l_key - 1)
    remainder = mod2div(appended_data, key)

    return remainder

def mod2div(divident, divisor):
    # Number of bits to be XORed at a time.
    pick = len(divisor)

    # Slicing the divident to appropriate
    # length for particular step
    tmp = divident[0: pick]

    while pick < len(divident):

        if tmp[0] == '1':

            # replace the divident by the result
            # of XOR and pull 1 bit down
            tmp = xor(divisor, tmp) + divident[pick]

        else:  # If leftmost bit is '0'
            # If the leftmost bit of the dividend (or the
            # part used in each step) is 0, the step cannot
            # use the regular divisor; we need to use an
            # all-0s divisor.
            tmp = xor('0' * pick, tmp) + divident[pick]

            # increment pick to move further
        pick += 1

    # For the last n bits, we have to carry it out
    # normally as increased value of pick will cause
    # Index Out of Bounds.
    if tmp[0] == '1':
        tmp = xor(divisor, tmp)
    else:
        tmp = xor('0' * pick, tmp)

    checkword = tmp
    return checkword