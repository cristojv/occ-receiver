from threading import Thread, Lock
import cv2
import numpy as np

class FrameStreamer:

    def __init__(self, framestreamer, isRealTime):
        self._framestreamer = framestreamer;
        self._streamer = cv2.VideoCapture(framestreamer)
        self._realTime = isRealTime
        for i in range(3):
            if not self._streamer.isOpened():
                print("The frame streamer is not available. Trying again ({})".format(i))
                self._streamer = cv2.VideoCapture(framestreamer)
                key = cv2.waitKey(1000)

        # Grabing first frame
        self._isFrameGrabbed, self._frame = self._streamer.read()

        # Initialize flags and thread variables
        self._isCapturing = False
        self._isFrameAvailable = True
        self._thread = 0
        self._readLock = Lock()

    def start(self):
        self._isCapturing = True
        if self._thread == 0:
            self._thread = Thread(target=self.update, args=())
            self._thread.start()
            return self

    def update(self):
        while (self._isCapturing):
            if(self.isFrameAvailable() and not self._realTime):
                pass
            else:
                isFrameGrabbed, frame = self._streamer.read()
                self._readLock.acquire()
                self._isFrameGrabbed, self._frame = isFrameGrabbed, frame
                self._isFrameAvailable = True
                self._readLock.release()
        self._streamer.release()
        print("The streamer has been stopped succesfully.")
        return

    def isCapturing(self):
        return self._isCapturing

    def isFrameAvailable(self):
        return self._isFrameAvailable

    def grabFrame(self):
        # if (self._realTime):
        #     self._readLock.acquire()
        #     frame = self._frame.copy()
        #     self._isFrameAvailable = False
        #     self._readLock.release()
        #
        #     frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        #     frame = frame.astype(np.float32) / 255.0
        #     # Gamma correction
        #     frame = frame ** (1.8)
        #     return self._isFrameGrabbed, frame
        # else:

        self._readLock.acquire()
        isFrameGrabbed = self._isFrameGrabbed
        if not isFrameGrabbed:
            return False, None

        frame = self._frame.copy()

        # self._isFrameGrabbed, self._frame = self._streamer.read()

        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        frame = frame.astype(np.float32) / 255.0
        # Gamma correction
        frame = frame ** (1.8)
        self._isFrameAvailable = False
        self._readLock.release()
        return isFrameGrabbed, frame

    def setFrameStart(self,number):
        totalFrames = self._streamer.get(cv2.CAP_PROP_FRAME_COUNT)
        if(number >= 0 & number <= totalFrames):
            self._streamer.set(cv2.CAP_PROP_POS_FRAMES,number)
            self._isFrameGrabbed, self._frame = self._streamer.read()

    def stop(self):
        self._isCapturing = False
        if self._thread.is_alive():
            self._thread.join()
        print("The streamer is going to be stopped")