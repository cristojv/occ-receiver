import cv2
import numpy as np


def resetCalibration(pattern, roiFrame, rowHeight, calibrationMeasurements):
    roiHeight = roiFrame.shape[0]
    calibrationMeasurements = np.empty((roiHeight, 3, 3))
    calibrationMeasurements[:] = np.nan

    startingRow = rowHeight // 2
    endingRow = startingRow + 1

    meanRoiFrame = np.mean(roiFrame.copy(), axis=1)

    # Fill Green Calibration Measurements
    for calibration in range(0, 3):
        indexes = np.where(pattern == calibration)[0]
        for index in indexes:
            calibrationMeasurements[int(startingRow + index * rowHeight):int(endingRow + index * rowHeight), :,
            calibration] = \
                (meanRoiFrame[int(startingRow + index * rowHeight):int(endingRow + index * rowHeight)])
    return calibrationMeasurements


def meassuring(roiPattern, roiFrame, rowHeight, packetTemplate, calibrationMeasurements, method=cv2.TM_CCOEFF_NORMED):
    result = cv2.matchTemplate(roiFrame, packetTemplate, method)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)
    topRow = max_loc[1]

    startingRow = rowHeight // 2 + topRow
    endingRow = startingRow + 1
    meanRoiFrame = np.mean(roiFrame.copy(), axis=1)

    # Fordward filling
    for calibration in range(0, 3):
        indexes = np.where(roiPattern == calibration)[0]
        for index in indexes:
            calibrationMeasurements[int(startingRow + index * rowHeight):int(endingRow + index * rowHeight), :,
            calibration] = \
                (meanRoiFrame[int(startingRow + index * rowHeight):int(endingRow + index * rowHeight)])
    # Backward filling
    backwardpattern = np.flip(roiPattern)
    for calibration in range(0, 3):
        indexes = np.where(backwardpattern == calibration)[0]
        indexes = indexes + 1
        for index in indexes:
            if (int(startingRow - index * rowHeight) > 0):
                calibrationMeasurements[int(startingRow - index * rowHeight):int(endingRow - index * rowHeight), :,
                calibration] = \
                    (meanRoiFrame[int(startingRow - index * rowHeight):int(endingRow - index * rowHeight)])
    return calibrationMeasurements

def fitting(calibrationMeasurements):
    return polinomialFittingStandard(calibrationMeasurements)

def polinomialFittingStandard(calibrationMeasurements):
    x = np.arange(len(calibrationMeasurements[:, 0]))
    idx0 = np.where(np.logical_not(np.isnan(calibrationMeasurements[:, 0])))[0]
    idx1 = np.where(np.logical_not(np.isnan(calibrationMeasurements[:, 1])))[0]
    idx2 = np.where(np.logical_not(np.isnan(calibrationMeasurements[:, 2])))[0]

    greenFitting = np.polyfit(x[idx1], calibrationMeasurements[idx1, 1], 4)
    blueFitting = np.polyfit(x[idx2], calibrationMeasurements[idx2, 2], 4)
    redFitting = np.polyfit(x[idx0], calibrationMeasurements[idx0, 0], 4)

    greenFitter = np.poly1d(greenFitting)
    blueFitter = np.poly1d(blueFitting)
    redFitter = np.poly1d(redFitting)

    greenAjustment = greenFitter(x)
    blueAjustment = blueFitter(x)
    redAjustment = redFitter(x)

    return np.concatenate((redAjustment.reshape(calibrationMeasurements.shape[0], 1), \
                           greenAjustment.reshape(calibrationMeasurements.shape[0], 1), \
                           blueAjustment.reshape(calibrationMeasurements.shape[0], 1)), axis=1)

def calibrateChannels(calibrationFitting):
    redFitting = calibrationFitting[:, :, 0]
    greenFitting = calibrationFitting[:, :, 1]
    blueFitting = calibrationFitting[:, :, 2]

    numberOfSamples = len(greenFitting)

    redNormalization = redFitting
    greenNormalization = greenFitting
    blueNormalization = blueFitting

    for i in range(0, numberOfSamples):
        currentMatrix = np.concatenate(
            (redNormalization[i, :].reshape(3, 1),
             greenNormalization[i, :].reshape(3, 1),
             blueNormalization[i, :].reshape(3, 1)), axis=1)
        diagonal = np.diag(np.diag(currentMatrix))
        currentMatrix = np.dot(currentMatrix, np.linalg.inv(diagonal)).reshape(1, 3, 3)
        try:
            inverse = np.linalg.inv(currentMatrix)
        except np.linalg.LinAlgError:
            print("Inverse matrix not computable")
            pass
        else:
            if (i == 0):
                invColorMatrix = inverse
            else:
                invColorMatrix = np.concatenate((invColorMatrix, inverse))

    thresholdCurve = np.concatenate((redFitting[:, 0].reshape(numberOfSamples, 1),
                                     greenFitting[:, 1].reshape(numberOfSamples, 1),
                                     blueFitting[:, 2].reshape(numberOfSamples, 1)), axis=1)
    return invColorMatrix, thresholdCurve

def computeThreshold(invColorMatrix,thresholdCurve):
    return enhancing(thresholdCurve,invColorMatrix)

def enhancing(roiFrame,invColorMatrix, roi = []):
    if not len(roi)==0:
        start = roi[0][1]
        stop = roi[1][1]
        index = 0
        for i in range (start,stop):
            colorAdaptedSample = np.dot(invColorMatrix[i],roiFrame[index]).reshape(1, 3)
            if index == 0:
                colorAdaptedSignal = colorAdaptedSample
            else:
                colorAdaptedSignal = np.concatenate((colorAdaptedSignal,colorAdaptedSample))
            index = index+1;
        colorAdaptedSignalFiltered = cv2.blur(colorAdaptedSignal,(1,3))
        colorAdaptedSignalFiltered[colorAdaptedSignalFiltered<=0] = 0
        return colorAdaptedSignalFiltered
    else:
        numberOfSamples,channels = roiFrame.shape
        colorAdaptedSignal = 0
        for i in range (0,numberOfSamples):
            colorAdaptedSample = np.dot(invColorMatrix[i],roiFrame[i]).reshape(1, 3)
            if i == 0:
                colorAdaptedSignal = colorAdaptedSample
            else:
                colorAdaptedSignal = np.concatenate((colorAdaptedSignal,colorAdaptedSample))

        colorAdaptedSignalFiltered = cv2.blur(colorAdaptedSignal,(1,3))
        colorAdaptedSignalFiltered[colorAdaptedSignalFiltered<=0] = 0
        return colorAdaptedSignalFiltered