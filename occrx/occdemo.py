from multiprocessing import Process, Queue
import queue
import cv2
import json
from occrx.framestreamer import FrameStreamer
from occrx.occParams import OCCSystemParams
from occrx.occreceiver import OCCReceiver

class OCCDemo(Process):
    def __init__(self, inputQueue):
        super(OCCDemo, self).__init__()
        self._inputQueue = inputQueue
        self._isRunning = False

    def run(self):
        self._isRunning = True
        while(self._isRunning):
            if(not self._inputQueue.empty()):
                frame = None
                try:
                    frame = self._inputQueue.get_nowait()

                    frame = cv2.resize(frame, (1920//3,1080//3))
                    cv2.imshow('frame2',frame)
                    cv2.waitKey(1)
                except queue.Empty:
                    print("HERE")
                    frame = None

class OCCDemo2(Process):
    def __init__(self,configParametersFilename, outputQueue):
        super(OCCDemo2, self).__init__()
        self._outputQueue = outputQueue
        self._isRunning = False

        self._occSystemParams = OCCSystemParams()

        with open(configParametersFilename) as read_file:
            self._occSystemParams.initFromJSON(json.load(read_file))

    def run(self):
        framestreamer = FrameStreamer(1, True, self._occSystemParams.occRxParams)
        self._isRunning = True
        framestreamer.start()
        while(self._isRunning):
            success, frame = framestreamer.grabFrame()
            if(success):
                try:
                    self._outputQueue.put_nowait(frame)
                except queue.Full:
                    pass

if __name__ == "__main__":
    frameQueue = Queue(50)
    inputQueue = Queue(20)
    outputQueue = Queue(2)
    occDemo1 = OCCDemo(frameQueue)
    occDemo2 = OCCReceiver(inputQueue,outputQueue,True,frameQueue,'C:/Users/cristojv/codinglab/python/occrx/logitechC920/logitechC920params.json')
    occDemo1.start()
    occDemo2.start()
