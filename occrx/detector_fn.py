import cv2
import numpy as np

def detectRoi(frame, template, threshold,method = cv2.TM_CCOEFF_NORMED):
    result = cv2.matchTemplate(frame, template, method)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)
    top_left = max_loc
    h, w, z = template.shape
    bottom_right = (top_left[0] + w, top_left[1] + h)
    roi= np.array((top_left, bottom_right))
    if(max_val >= threshold):
        return True,top_left,max_val,min_loc,min_val,roi
    return False,top_left,max_val,min_loc,min_val,roi