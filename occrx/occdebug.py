from multiprocessing import Process, Queue
# from occrx.occreceiver import OCCRxExternalEvents
from enum import Enum
import queue
import cv2
import matplotlib

class BaseEvent():
    def __init__(self, id):
        self._id = id

    def getId(self):
        return self._id
class OCCRxExternalEvents(BaseEvent):
    class Events(Enum):
        OCCRx_LAUNCH = 1
        OCCRx_STOP = 2
    def __init__(self,id):
        super(OCCRxExternalEvents, self).__init__(id)



