import math

# @brief: computeRealDistance
# params: 	vFOV: vertical Field Of View of the camera in degrees
# 			vResolution: vertical resolution in pixel of the camera
#			dHeight: vertical distance in meters
#			yHeight: vertical heigth in pixels

def computeRealDistance(vFOV,vResolution,dHeight,yHeight):
    phiY = vResolution/math.radians(vFOV)
    distance = dHeight/(2*math.tan(yHeight * math.radians(vFOV)/2/vResolution))
    return distance