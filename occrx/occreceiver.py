#import the necessary packages
from occrx.framestreamer import FrameStreamer, adaptFrame
from occrx.detector import Detector
from occrx.calibrator import Calibrator
from occrx.demodulator import Demodulator
import occrx.templates
# from unpacker import unpack
# from events.cameraevents import CameraEvent, CameraEvents
# from events.coordinatorevents import CoordinatorEvent, CoordinatorCameraEvent, CoordinatorEvents
import cv2
import numpy as np

from enum import Enum
from multiprocessing import Process, Queue
import queue
import time
import json
from occrx.occParams import OCCSystemParams, OCCTxParams, OCCRxParams

class CameraStates(Enum):
    INIT = 1
    SOURCING = 2
    CALIBRATING = 3
    RECEIVING_START = 4
    RECEIVING_DATA = 5


class BaseEvent():
    def __init__(self, id):
        self._id = id

    def getId(self):
        return self._id

class CameraEvents(Enum):
    CAMERA_LAUNCH = 1
    CAMERA_CONFIG = 2
    CAMERA_STOP = 3
    CAMERA_CHECK = 4
    CAMERA_NEWFRAME = 5

class CameraEvent(BaseEvent):
    pass

class OCCReceiver(Process):
    def __init__(self, inputQueue, outputQueue, isDemo, frameOutputQueue, configParametersFilename):
        super(OCCReceiver, self).__init__()
        self._inputQueue = inputQueue
        self._outputQueue = outputQueue
        self._isDemo = isDemo
        self._frameOutputQueue = frameOutputQueue
        self._occSystemParams = occrx.occParams.OCCSystemParams()

        with open(configParametersFilename) as read_file:
            self._occSystemParams.initFromJSON(json.load(read_file))

    def run(self):
        '''*****************************************
        Initializes variables
        *****************************************'''

        c_numberOfRows = 5 #packet lenght in number of bands
        c_numberOfRoiRows = 2 * c_numberOfRows + 1
        c_rowHeight = -(-self._occSystemParams.occRxParams.resolution[0]*9*self._occSystemParams.occRxParams.samplingFrequency//(16*self._occSystemParams.occTxParams.txFrequency))#number of row pixels per band
        c_columnHeight = 7 #number of column pixels considered for correlation

        '''*****************************************
        Global state variables
        *****************************************'''
        g_frame = None
        g_roi = None
        g_packet = None
        g_roiPoints = None
        g_sroiPoints = None
        g_state = CameraStates.INIT
        g_isRunning = True
        g_frameProcessed = False
        g_somethingDetected = False
        g_response = None
        g_packetByte = None
        g_roiFound = False
        g_MAXSourcingIteration = 5
        g_sourcingIteration = 0
        g_sroiFound = False
        g_MAXsroiIteration = 30
        g_sroiIteration = 0
        g_MAXCalibratingIteration = 100
        g_calibratingIteration = 0

        #Generate patterns
        baseRoiPattern = self._occSystemParams.occTxParams.patterns.roiPattern
        baseSyncPattern = self._occSystemParams.occTxParams.patterns.syncPattern
        roiPattern = occrx.templates.generateFinalPattern(baseRoiPattern,c_numberOfRoiRows)
        packetPattern = occrx.templates.generateFinalPattern(baseRoiPattern,c_numberOfRows)
        syncPattern = occrx.templates.generateFinalPattern(baseSyncPattern,c_numberOfRows)
        calibrationPattern = occrx.templates.generateFinalPattern(baseRoiPattern,c_numberOfRows * 2)

        #Generate templates
        roiTemplate = occrx.templates.generateTemplate(pattern=roiPattern,rowHeight=c_rowHeight,columnHeight=c_columnHeight)
        packetTemplate = occrx.templates.generateTemplate(pattern=packetPattern,rowHeight=c_rowHeight,columnHeight=c_columnHeight)
        syncTemplate = occrx.templates.generateTemplate(pattern=syncPattern,rowHeight=c_rowHeight,columnHeight=c_columnHeight)
        calibrationTemplate = occrx.templates.generateTemplate(pattern=calibrationPattern,rowHeight=c_rowHeight,columnHeight=c_columnHeight)

        framestreamer = FrameStreamer(1,True,self._occSystemParams.occRxParams)

        #Instanciate modules
        # detector = Detector(rowHeight=c_rowHeight,numberOfRows=c_numberOfRows,columnHeight=c_columnHeight)
        # calibrator = Calibrator(rowHeight=c_rowHeight,numberOfRows=c_numberOfRows,columnHeight=c_columnHeight,numberOfFramesForCalibration=100)
        # demodulator = Demodulator()
        isRunning = True
        #TODO
        self._inputQueue.put(CameraEvent(CameraEvents.CAMERA_LAUNCH))

        while(isRunning):
            time.sleep(0.01);
            print(g_state)
            if(g_state == CameraStates.INIT):
                '''*****************************************
                Init state
                *****************************************'''
                if(self._inputQueue.empty()):
                    pass
                else:
                    cameraEvent = self._inputQueue.get()
                    if isinstance(cameraEvent, CameraEvent):
                        if cameraEvent.getId() == CameraEvents.CAMERA_LAUNCH:
                            g_frameProcessed = False;
                            g_somethingDetected = None;
                            g_roiFound = False;
                            g_sourcingIteration = 0;
                            g_sroiFound = False;
                            g_sroiIteration = 0;
                            g_calibratingIteration = 0;
                            g_state = CameraStates.SOURCING;
                            framestreamer.start()
                            pass
            # elif(g_state == CameraStates.SOURCING):
            #         '''*****************************************
            #         Sourcing state
            #         *****************************************'''
            #         if(g_frameProcessed):
            #
            #                 if(g_roiFound):
            #                         g_sourcingIteration = 0;
            #                         g_frameProcessed = False;
            #                         g_state = CameraStates.CALIBRATING;
            #                         framestreamer.flush()
            #                         #g_roi = g_frame[g_roiPoints[0][1]:g_roiPoints[1][1],g_roiPoints[0][0]:g_roiPoints[1][0]]
            #                 else:
            #
            #                         if(g_sourcingIteration < g_MAXSourcingIteration):
            #                                 g_sourcingIteration += 1
            #                                 g_frameProcessed = False
            #                                 g_state = CameraStates.SOURCING
            #                                 framestreamer.flush()
            #                         else:
            #                                 g_sourcingIteration=0;
            #                                 g_frameProcessed = False;
            #                                 g_state = CameraStates.INIT;
            #                                 framestreamer.stop()
            #                                 framestreamer.flush()
            #                                 # self._outputQueue.put(CoordinatorEvent(CoordinatorEvents.CAMERA_ROINOTFOUND))
            elif(g_state == CameraStates.CALIBRATING):
                    '''*****************************************
                    Calibrating state
                    *****************************************'''
                    if(g_frameProcessed):
                            if(g_sroiFound):
                                    if(g_calibratingIteration<g_MAXCalibratingIteration):
                                            g_calibratingIteration += 1
                                            g_frameProcessed = False
                                            g_sroiFound = False
                                            g_sroiIteration = 0
                                            g_state = CameraStates.CALIBRATING;
                                            # calibrator.calibrateChannels(g_roi,g_sroiPoints)
                                            #TODO Insert Data for Calibration
                                    else:
                                            g_calibratingIteration = 0
                                            g_frameProcessed = False
                                            g_sroiFound = False
                                            g_sroiIteration = 0
                                            g_state = CameraStates.RECEIVING_START;
                                            framestreamer._camera.stop_preview() #TODO remove
                                            # self._outputQueue.put(CoordinatorEvent(CoordinatorEvents.CAMERA_ROIFOUND))
                                            # calibrator.polinomialFittingCalibration()
                                            #TODO Polynomial Fitting for Calibration
                            else:
                                    if(g_sroiIteration < g_MAXsroiIteration):
                                            g_calibratingIteration = 0
                                            g_frameProcessed = False
                                            g_sroiFound = False
                                            g_sroiIteration += 1
                                            g_state = CameraStates.CALIBRATING;
                                    else:
                                            g_frameProcessed = False;
                                            g_somethingDetected = None;
                                            g_roiFound = False;
                                            g_sourcingIteration = 0;
                                            g_sroiFound = False;
                                            g_sroiIteration = 0;
                                            g_calibratingIteration = 0;
                                            g_state = CameraStates.INIT;
                                            framestreamer.stop()
                                            framestreamer.flush()
                                            # self._outputQueue.put(CoordinatorEvent(CoordinatorEvents.CAMERA_ROINOTFOUND))
            elif(g_state == CameraStates.RECEIVING_START):
                    '''*****************************************
                    Receiving state
                    *****************************************'''
                    #print("Receiving State")
                    if(g_frameProcessed):
                        if(g_somethingDetected):
                            if not (g_packetByte==None):
                                # startPacket = unpack(g_packetByte)
                                # if(startPacket.bit.response == 1): #ACK
                                #     self._outputQueue.put(CoordinatorCameraEvent(CoordinatorEvents.CAMERA_RESPONSEOK,\
                                #                           startPacket.bit.id,\
                                #                           startPacket.bit.response,\
                                #                           None))
                                    g_sroiIteration = 0
                                    g_state = CameraStates.RECEIVING_START
                                    g_somethingDetected = False


                        g_frameProcessed = False

            # time.sleep(0.01);
            if(framestreamer.isCapturing()):
                    #print(g_state)
                    '''1) Grab a new frame
                    2) Detection algorithm
                    2.1)SOURCE: If there is NO source detected. Find a new Source and lock it.
                    If there is source locked. Proceed to correlate packets.
                    2.2)DATA:   Using the ROI computed previously, determine if
                    there is data being sent. If there is data proceed to
                    demodulate the signal.
                    2.3)LOCK:   If there is NO data being sent, check if the source is
                    still locked.
                    '''
                    success,g_frame =framestreamer.grabFrame()
                    #print("No hay frame")
                    if g_frame is not None:
                        g_frame = adaptFrame(g_frame)
                        try:
                            self._frameOutputQueue.put_nowait(g_frame)
                        except queue.Full:
                            pass

                        print("Cogiendo frame")
                        # g_frameProcessed = True
                        # if (g_state == CameraStates.INIT):
                        #     pass
                        #
                        # elif(g_state == CameraStates.SOURCING):
                        #     g_roiFound, g_roiPoints = detector.findSource(g_frame)
                        #     print(g_roiPoints)
                        #
                        # elif(g_state == CameraStates.CALIBRATING):
                        #     g_roi = g_frame[g_roiPoints[0][1]:g_roiPoints[1][1],g_roiPoints[0][0]:g_roiPoints[1][0]]
                        #     g_roi = cv2.GaussianBlur(g_roi,(5,5),0)
                        #     g_sroiFound, g_sroiPoints = detector.findSRoi(g_roi)
                        #     print(g_sroiFound)
                        # elif(g_state == CameraStates.RECEIVING_START):
                        #     g_roi = g_frame[g_roiPoints[0][1]:g_roiPoints[1][1],g_roiPoints[0][0]:g_roiPoints[1][0]]
                        #     g_roi = cv2.GaussianBlur(g_roi,(5,5),0)
                        #     packetFound, packetPoints = detector.findPacket(g_roi)
                        #     if packetFound:
                        #         #print("Something has been detected")
                        #         packet = g_roi[packetPoints[0][1]:packetPoints[1][1],packetPoints[0][0]:packetPoints[1][0]]
                        #         inv_color, inv_space = calibrator.getChannelsCalibration()
                        #         inv_color_roi = inv_color[packetPoints[0][1]:packetPoints[1][1],packetPoints[0][0]:packetPoints[1][0]];
                        #         inv_space_roi = inv_space[packetPoints[0][1]:packetPoints[1][1],packetPoints[0][0]:packetPoints[1][0]];
                        #         thresholdLevel = calibrator.getChannelThreshold()
                        #         thresholdLevel = thresholdLevel[packetPoints[0][1]:packetPoints[1][1],packetPoints[0][0]:packetPoints[1][0]];
                        #         packetMean = np.mean(packet, axis=1)
                        #         status, enhancedSignal = demodulator.enhanceSignal(packetMean, inv_color_roi, inv_space_roi)
                        #         '''
                        #         figure = plt.figure();
                        #         graph = figure.add_subplot(311)
                        #         graph1 = figure.add_subplot(312)
                        #         graph.plot(enhancedSignal[:, 0], 'b');
                        #         graph.plot(enhancedSignal[:, 1], 'g');
                        #         graph.plot(enhancedSignal[:, 2], 'r');
                        #         graph.plot(thresholdLevel[:,0],'b');
                        #         graph.plot(thresholdLevel[:,1],'g');
                        #         graph.plot(thresholdLevel[:,2],'r');
                        #         plt.show()'''
                        #         status,id,demodulatedBits = demodulator.demodulateSignal(enhancedSignal.astype(np.uint8),thresholdLevel)
                        #         if(status==1):
                        #             statusDemodulation,charv = demodulator.convertByteToChar(demodulatedBits)
                        #             g_packetByte = charv
                        #             g_somethingDetected = True
                        #             #print("id: {}, m: {}, b: {}".format(id,demodulatedBits,charv))
                        #             #f.write("{}, ".format(str(charv)))
                        #             #somethingIsFound+=1
                        #         elif(status == 3):
                        #             statusDemodulation,charv = demodulator.convertByteToChar(demodulatedBits)
                        #             #print("id: {}, m: {}, b: {} (With error)".format(id,demodulatedBits,chr(charv)))
                        #             #f.write(" {(E)} ")
                        #             #f.write(str(charv))
                        #             #f.write(" {(E)} ")
                        #             #somethingIsFound+=1
                        #         #print("Count: {}".format(somethingIsFound))
                        #         #if(somethingIsFound==1000):
                        #             #camera.stop()
                        #             #isRunning = False
                        #             #f.flush()
                        #             #f.close()
                        #     else:
                        #         g_sroiFound, g_sroiPoints = detector.findSRoi(g_roi)
        isRunning = False
        
if __name__ == "__main__":
    occReceiver = OCCReceiver()
    occReceiver.start()
    while not occReceiver.camera.isRunning():
        #print(occReceiver.camera.isRunning())
        pass
    occReceiver.stop()
    occReceiver.join()
    print('Has been joined')
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    