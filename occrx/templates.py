import numpy as np

def generateUnitTemplate(patternUnit):
    if(patternUnit == 0):
        return np.array([[[1, 0, 0]]])
    elif(patternUnit == 1):
        return np.array([[[0, 1, 0]]])
    elif(patternUnit == 2):
        return np.array([[[0, 0, 1]]])
    else:
        return np.array([[[0, 0, 0]]])

def generateFinalPattern(pattern,numberOfRows):
    finalPattern = np.array(pattern[0]).reshape(1)
    for i in range(1,numberOfRows):
        j = i % len(pattern)
        finalPattern = np.append(finalPattern,pattern[j])
    return finalPattern

def generateTemplate(pattern, rowHeight, columnHeight):
    template = np.repeat(generateUnitTemplate(pattern[0]), rowHeight, axis=0)
    for i in range(1,len(pattern)):
        template = np.concatenate((template,np.repeat(generateUnitTemplate(pattern[i]), rowHeight, axis=0)),axis=0)
    template = np.repeat(template, columnHeight, axis=1)
    return template.astype(np.float32)

def generateTemplateForNRows(pattern, rowHeight, columnHeight, numberOfRows):
    finalPattern = generateFinalPattern(pattern,numberOfRows)
    return generateTemplate(finalPattern, rowHeight, columnHeight)

def generateSyncTemplate(rowHeight):
    template_g = np.repeat(255, rowHeight, axis=0)
    template_n = np.repeat(0, rowHeight, axis=0)
    template = np.concatenate((template_g,template_g,template_n,template_g,template_n),axis=0)
    return template.astype(np.float32)/255.0
