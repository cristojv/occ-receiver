import sys
sys.path.append("..")
from jsonserializable import JSONSerializable

class OCCTxParams(JSONSerializable):
    class Patterns:
        def __init__(self, roiPattern=None, syncPattern=None):
            self.roiPattern = roiPattern
            self.syncPattern = syncPattern

    def __init__(self, txFrequency=None, patterns=Patterns()):
        self.txFrequency = txFrequency
        self.patterns = patterns

    def toJSON(self):
        data = {
            "txFreq":float(self.txFrequency),
            "patterns": {
                "roi":self.patterns.roiPattern,
                "sync":self.patterns.syncPattern
            }
        }
        return data

    def initFromJSON(self,jsonData):
        self.txFrequency = jsonData["txFreq"]
        self.patterns.roiPattern = jsonData["patterns"]["roi"]
        self.patterns.syncPattern = jsonData["patterns"]["sync"]

class OCCChannelParams(JSONSerializable):
    def __init__(self, distance=0):
        self.distance = distance

    def toJSON(self):
        data = {
            "distance":float(self.distance),
        }
        return data

    def initFromJSON(self,jsonData):
        self.distance = jsonData["distance"]

class OCCRxParams(JSONSerializable):
    def __init__(self,
                 resolution=None,
                 gain=None,
                 whitebalance=None,
                 exposure=None,
                 samplingFrequency=None,
                 rowHeight=None):
        self.resolution = resolution
        self.gain = gain
        self.whitebalance = whitebalance
        self.exposure = exposure
        self.samplingFrequency = samplingFrequency
        self.rowHeight = rowHeight

    def toJSON(self):
        data = {
            "resolution":list(map(int,self.resolution)),
            "gain":float(self.gain),
            "whitebalance":float(self.whitebalance),
            "exposure":float(self.exposure),
            "fps":float(self.samplingFrequency),
            "rowHeight":int(self.rowHeight)
        }
        return data

    def initFromJSON(self,jsonData):
        self.resolution = jsonData["resolution"]
        self.gain = jsonData["gain"]
        self.whitebalance = jsonData["whitebalance"]
        self.exposure = jsonData["exposure"]
        self.samplingFrequency = jsonData["fps"]
        self.rowHeight = jsonData["rowHeight"]

class OCCSystemParams(JSONSerializable):
    def __init__(self, occTxParams=OCCTxParams(), occChannelParams=OCCChannelParams, occRxParams=OCCRxParams):
        self.occTxParams = occTxParams
        self.occChannelParams = occChannelParams
        self.occRxParams = occRxParams

    def toJSON(self):
        data = {
            "occTx": self.occTxParams.toJSON(),
            "occChannel": self.occChannelParams.toJSON(),
            "occRx": self.occRxParams.toJSON()
        }
        return data

    def initFromJSON(self, jsonData):
        self.occTxParams = OCCTxParams()
        self.occTxParams.initFromJSON(jsonData["occTx"])
        self.occChannelParams = OCCChannelParams()
        self.occChannelParams.initFromJSON(jsonData["occChannel"])
        self.occRxParams = OCCRxParams()
        self.occRxParams.initFromJSON(jsonData["occRx"])


class OCCTestParams(JSONSerializable):
    def __init__(self, columnHeight=None,
                 numberOfFramesForCalibration=None,
                 numberOfRoiRows=None,
                 numberOfPacketRows=None,
                 roiThresholdLevel=None):
        self.columnHeight = columnHeight
        self.numberOfFramesForCalibration = numberOfFramesForCalibration
        self.numberOfRoiRows = numberOfRoiRows
        self.numberOfPacketRows = numberOfPacketRows
        self.roiThresholdLevel = roiThresholdLevel

    def toJSON(self):
        data = {
            "columnHeight": int(self.columnHeight),
            "framesForCalibration": int(self.numberOfFramesForCalibration),
            "numberOfRoiRows": int(self.numberOfRoiRows),
            "numberOfPacketRows": int(self.numberOfPacketRows),
            "roiThresholdLevel": float(self.roiThresholdLevel)
        }
        return data

    def initFromJSON(self, jsonData):
        self.columnHeight = jsonData["columnHeight"]
        self.numberOfFramesForCalibration = jsonData["framesForCalibration"]
        self.numberOfRoiRows = jsonData["numberOfRoiRows"]
        self.numberOfPacketRows = jsonData["numberOfPacketRows"]
        self.roiThresholdLevel = jsonData["roiThresholdLevel"]