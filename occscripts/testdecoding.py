import sys
sys.path.append("..")
import numpy as np
import cv2
import matplotlib.pyplot as plt
from occrx.framestreamer import FrameStreamer
import occrx.templates
import occrx.detector
import occrx.calibrator
import occrx.demodulator
import occrx.occParams
import json
from jsonserializable import JSONSerializable
import time
from occscripts.testdetection import DetectionResults
from occscripts.testcalibration import CalibrationResults

class DecodingResults(JSONSerializable):
    def __init__(self, roiPoint = None,
                 PER = None,
                 valids = None,
                 errors = None,):

        self.PER = PER
        self.valids = valids
        self.errors = errors

    def toJSON(self):
        data = {
            "PER": float(self.PER),
            "valids": int(self.valids),
            "errors": int(self.errors)
        }
        return data

    def initFromJSON(self, jsonData):
        self.PER = float(jsonData["PER"])
        self.valids = int(jsonData["valids"])
        self.errors = int(jsonData["errors"])

class DataResults(JSONSerializable):
    def __init__(self, binaryData = None):
        self._binaryData = binaryData
    def toJSON(self):
        data = {
            "binaryData": json.dumps(self._binaryData, separators=(',', ':'), sort_keys=True, indent=4)
        }
        return data
    def initFromJSON(self,jsonData):
        self._binaryData = np.array(json.loads(jsonData["binaryData"]))

def testPER4NumberOfCalibrations(videoData, occSystemParams, occTestParams, detectionResults, calibrationResults, baseDirectoryFilename):
    tic = time.clock()
    testDirectory = baseDirectoryFilename
    errorDirectory = testDirectory +"/error"
    os.makedirs(errorDirectory)

    print('Init Test for: {}'.format(testDirectory))

    # *****************************************************************************
    # GENERATING VARIABLES
    # *****************************************************************************

    dataResults = DataResults()
    decimaData = []
    # decodingResults = DecodingResults()
    # PER = None
    # valids = 0
    # errors = 0

    roiPattern = occrx.templates.generateFinalPattern(occSystemParams.occTxParams.patterns.roiPattern,
                                                      occTestParams.numberOfRoiRows)
    packetPattern = occrx.templates.generateFinalPattern(occSystemParams.occTxParams.patterns.roiPattern,
                                                         occTestParams.numberOfPacketRows)
    syncPattern = occrx.templates.generateFinalPattern(occSystemParams.occTxParams.patterns.syncPattern,
                                                         occTestParams.numberOfPacketRows)
    calibrationPattern = occrx.templates.generateFinalPattern(occSystemParams.occTxParams.patterns.roiPattern,
                                                         occTestParams.numberOfPacketRows*2)
    print(roiPattern)
    print(packetPattern)
    print(calibrationPattern)
    # ============================================================================
    # Correlation templates
    # ============================================================================

    roiTemplate = occrx.templates.generateTemplate(pattern=roiPattern,
                                                   rowHeight=occSystemParams.occRxParams.rowHeight,
                                                   columnHeight=occTestParams.columnHeight)
    syncTemplate = occrx.templates.generateTemplate(pattern=syncPattern,
                                                   rowHeight=occSystemParams.occRxParams.rowHeight,
                                                   columnHeight=occTestParams.columnHeight)
    syncTemplate1D = occrx.templates.generateSyncTemplate(rowHeight=occSystemParams.occRxParams.rowHeight)

    # ============================================================================
    # Calibration variables
    # ============================================================================
    roiHeight = roiTemplate.shape[0]

    # *****************************************************************************
    # STARTING - First Acquisition
    # *****************************************************************************

    streamer = FrameStreamer(videoData, isRealTime=False)
    streamer.start()
    isGrabbed,frame = streamer.grabFrame()

    fig, ajustAx = plt.subplots(3, 1)

    ajustAx[0].title.set_text('Green channel interferences')
    ajustAx[0].plot(calibrationResults.measuredValues[:, 0, 1], c='#a7adba')
    ajustAx[0].plot(calibrationResults.measuredValues[:, 1, 1], c='#293336')
    ajustAx[0].plot(calibrationResults.measuredValues[:, 2, 1], c='#a7adba')


    ajustAx[0].plot(calibrationResults.predictedValues[:, 0, 1], c='r')
    ajustAx[0].plot(calibrationResults.predictedValues[:, 1, 1], c='g')
    ajustAx[0].plot(calibrationResults.predictedValues[:, 2, 1], c='b')

    ajustAx[1].title.set_text('Red channel interferences')
    ajustAx[1].plot(calibrationResults.measuredValues[:, 0, 0], c='#293336')
    ajustAx[1].plot(calibrationResults.measuredValues[:, 1, 0], c='#a7adba')
    ajustAx[1].plot(calibrationResults.measuredValues[:, 2, 0], c='#a7adba')

    ajustAx[1].plot(calibrationResults.predictedValues[:, 0, 0], c='r')
    ajustAx[1].plot(calibrationResults.predictedValues[:, 1, 0], c='g')
    ajustAx[1].plot(calibrationResults.predictedValues[:, 2, 0], c='b')

    ajustAx[2].title.set_text('Blue channel interferences')
    ajustAx[2].plot(calibrationResults.measuredValues[:, 0, 2], c='#a7adba')
    ajustAx[2].plot(calibrationResults.measuredValues[:, 1, 2], c='#a7adba')
    ajustAx[2].plot(calibrationResults.measuredValues[:, 2, 2], c='#293336')

    ajustAx[2].plot(calibrationResults.predictedValues[:, 0, 2], c='r')
    ajustAx[2].plot(calibrationResults.predictedValues[:, 1, 2], c='g')
    ajustAx[2].plot(calibrationResults.predictedValues[:, 2, 2], c='b')
    fig.savefig(testDirectory + '/calibration.png')  # save the figure to file
    plt.close(fig)

    streamer = FrameStreamer(videoData, isRealTime=False)
    streamer.start()
    countFrames = 0
    while(isGrabbed):
        # tac = time.clock()
        # tom = time.clock()
        isGrabbed, frame = streamer.grabFrame()
        # tim = time.clock()
        # print("Grabbed frame in {}".format(tim-tom))
        if isGrabbed:
            # Mask ROI
            roiFrame = frame[calibrationResults.roiPoint[0][1]:calibrationResults.roiPoint[1][1], calibrationResults.roiPoint[0][0]:calibrationResults.roiPoint[1][0], :].copy()
            # roiFrame = np.mean(roiFrame, axis=1)

            # *****************************************************************************
            # DETECTION
            # *****************************************************************************
            status,top_left,max_val,min_loc,min_val,packetRoi = occrx.detector.detectRoi(roiFrame, syncTemplate,threshold=0)
            if(status):
                packetFrame = roiFrame[packetRoi[0][1]:packetRoi[1][1],packetRoi[0][0]:packetRoi[1][0], :]
                packetFrame = np.mean(packetFrame, axis=1)
                # *****************************************************************************
                # ENHANCING
                # *****************************************************************************

                # tom = time.clock()
                colorAdaptedSignalFiltered = occrx.calibrator.enhancing(packetFrame, calibrationResults.inverseMatrix, roi=packetRoi)
                # tim = time.clock()
                # print("Enhancing frame in {}".format(tim-tom))

                # *****************************************************************************
                # BINARIZING
                # *****************************************************************************

                binarizeSignal = occrx.demodulator.binarize(colorAdaptedSignalFiltered, calibrationResults.enhancedThreshold, roi=packetRoi)

                # *****************************************************************************
                # DECODING
                # *****************************************************************************

                statusDecoding, data = occrx.demodulator.decode(binarizeSignal, syncTemplate1D, threshold=0.8)
                data = (data.flatten()).astype(np.int)
                decimal = int(''.join(map(str, (data[0:10]))),2)
                decimaData.append(decimal)
                print("The decimal is: {}".format(decimal))
            # *****************************************************************************
            # TESTING
            # *****************************************************************************
            # isValid = occrx.demodulator.checkCRC(''.join(map(str, (data[0:10]))), '0101')

            # if not isValid:
            #     errors=errors+1
                # print("Errors: {}".format(errors))
                # if(errors < 10):
                #     cv2.imwrite(errorDirectory+"/error{}.jpg".format(errors),(frame**(1/1.8)*255))
            # else:
            #     valids=valids+1
                # print("Valids: {}".format(valids))
            countFrames = countFrames + 1
            if(countFrames%1000==0):
                countFrames = 0
                # print("Saving with valids:{}, and errors:{}".format(valids,errors))
                # decodingResults.valids = valids
                # decodingResults.errors = errors
                # print("Number of valids: {}".format(valids))
                # print("Number of errors: {}".format(errors))
                # print("PER: {}".format(valids))
                # if (valids > 0):
                #     decodingResults.PER = float(errors) / (float(valids)+float(errors))
                # else:
                #     decodingResults.PER = 1.0

                # print("PER: {}".format(decodingResults.PER))
                print("Saving decimal data")
                dataResults._binaryData = decimaData
                with open(testDirectory + '/dataresults.json', "w") as write_file:
                    json.dump(dataResults.toJSON(), write_file)
        # tactac = time.clock()
        # print("Time per sample: {}".format(tactac-tac))
    # decodingResults.valids = valids
    # decodingResults.errors = errors
    # print("Number of valids: {}".format(valids))
    # print("Number of errors: {}".format(errors))
    # print("PER: {}".format(valids))
    # if(valids>0):
    #     decodingResults.PER = float(errors) / (float(valids)+float(errors))
    # else:
    #     decodingResults.PER = 1.0
    #
    # print("PER: {}".format(decodingResults.PER))
    dataResults._binaryData = decimaData
    with open(testDirectory + '/dataresults.json', "w") as write_file:
        json.dump(dataResults.toJSON(), write_file)
    toc = time.clock()
    streamer.stop()
    print("Finished test after {} seconds".format(toc-tic))

if __name__ == '__main__':
    import os
    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument("-f", "--filename",type=str, required=True, help="base directory where the videos are stored")
    ap.add_argument("-tc", "--testCalibrations",type=str, required=True, help="list of test number of calibrations separated by commas 0,1,2")
    ap.add_argument("-dt", "--testDetection",type=str, required=True, help="the detection test number.")

    args = vars(ap.parse_args())
    filename = str(args["filename"])
    testCalibrationNumbers = list(map(int,(args["testCalibrations"].split(","))))
    detectionTestNumber = int(args["testDetection"])

    print("filename {}, type {}".format(filename, type(filename)))
    print("testCalibrations {}, type {}".format(testCalibrationNumbers, type(testCalibrationNumbers)))
    print("detectionTestNumber {}, type {}".format(detectionTestNumber, type(detectionTestNumber)))

    for file in os.listdir(filename):
        if not os.path.isfile(filename+"{}/test.ignore".format(file)):
            if os.path.exists(filename + "" + file + "/calibration/"):
                for calFile in os.listdir(filename + "" + file + "/calibration/"):
                    for trueFalseFile in os.listdir(filename + "" + file + "/calibration/"+calFile):
                        testFilename = filename + "" + file + "/calibration/"+calFile +"/"+trueFalseFile
                        if not os.path.isfile(testFilename + "/test.ignore"):
                            if not os.path.isfile(testFilename + '/decodingresults.json'):
                                print(testFilename)

                                occSystemParams = occrx.occParams.OCCSystemParams()
                                with open(testFilename+"/occsystemparams.json", "r") as read_file:
                                    occSystemParams.initFromJSON(json.load(read_file))

                                occTestParams = occrx.occParams.OCCTestParams()
                                with open(testFilename+"/occtestparams.json", "r") as read_file:
                                    occTestParams.initFromJSON(json.load(read_file))

                                detectionResults = DetectionResults()
                                with open(testFilename+"/detectionresults.json", "r") as read_file:
                                    detectionResults.initFromJSON(json.load(read_file))

                                calibrationResults = CalibrationResults()

                                with open(testFilename+"/calibrationresults.json", "r") as read_file:
                                    calibrationResults.initFromJSON(json.load(read_file))

                                testPER4NumberOfCalibrations(filename + "" + file + '/data.avi',
                                                             occSystemParams,
                                                             occTestParams,
                                                             detectionResults,
                                                             calibrationResults,
                                                             testFilename)