import sys
sys.path.append("..")
from occrx.framestreamer import FrameStreamer
import occrx.detector
import occrx.occParams
import occrx.templates
import json
from jsonserializable import JSONSerializable
import time
import occscripts.occscriptutils
from occscripts.occscriptutils import Circle, Rectangle

class DetectionParams(JSONSerializable):
    def __init__(self,occSystemParams=None,occConfParams=None,detectionCircle=None):
        self.occSystemParams = occSystemParams
        self.occConfParams = occConfParams
        self.detectionCircle = detectionCircle

    def toJSON(self):
        data = {
            "occSystemParams": self.occSystemParams.toJSON(),
            "occConfParams": self.occConfParams.toJSON(),
            "detectionCircle": self.detectionCircle.toJSON()
        }
        return data

    def initFromJSON(self, jsonData):
        self.occSystemParams = occrx.occParams.OCCSystemParams()
        self.occSystemParams.initFromJSON(jsonData["occSystemParams"])
        self.occConfParams = occrx.occParams.OCCConfParams()
        self.occConfParams.initFromJSON(jsonData["occConfParams"])
        self.detectionCircle = occscripts.occscriptutils.Circle()
        self.detectionCircle.initFromJSON(jsonData["circle"])

class DetectionResults(JSONSerializable):
    def __init__(self, isWithinCircle = None,
                 maxValues = None,
                 maxLoc = None,
                 minValues = None,
                 minLoc = None):
        self.isWithinCircle = isWithinCircle
        self.maxValues = maxValues
        self.maxLoc = maxLoc
        self.minValues = minValues
        self.minLoc = minLoc

    def toJSON(self):
        data = {
            "MaxPoints": list(list(x) for x in self.maxLoc),
            "MaxValues": list(map(float,self.maxValues)),
            "isWithinCircle": list(map(bool,self.isWithinCircle)),
            "MinPoints": list(list(x) for x in self.minLoc),
            "MinValues": list(map(float,self.minValues))
        }
        return data

    def initFromJSON(self, jsonData):
        self.isWithinCircle = jsonData["isWithinCircle"]
        self.maxValues = jsonData["MaxValues"]
        self.maxLoc = jsonData["MaxPoints"]
        self.minValues = jsonData["MinValues"]
        self.minLoc = jsonData["MinPoints"]

def testPacketDetection(baseFolder,testDirectory, occSystemParams, occTestParams):
    tic = time.clock()
    '''
    1) OpenFolder
    2) Load Circle
    3) Load TestParams
    '''

    videoFile = baseFolder+'/calibration.avi'

    circle = Circle()

    with open(baseFolder + "circle.json", "r") as read_file:
        circle.initFromJSON(json.load(read_file))
    with open(testDirectory + '/circle.json', "w") as write_file:
        json.dump(circle.toJSON(), write_file)

    height = occSystemParams.occRxParams.rowHeight*occTestParams.numberOfRoiRows
    width = occTestParams.columnHeight

    rectangle = Rectangle(topLeftCornerPoint=(0,0),width=width,height=height)

    # *****************************************************************************
    # GENERATING VARIABLES
    # *****************************************************************************

    roiPattern = occrx.templates.generateFinalPattern(occSystemParams.occTxParams.patterns.roiPattern,
                                                      occTestParams.numberOfRoiRows)
    # ============================================================================
    # Correlation templates
    # ============================================================================

    roiTemplate = occrx.templates.generateTemplate(pattern=roiPattern,
                                                   rowHeight=occSystemParams.occRxParams.rowHeight,
                                                   columnHeight=occTestParams.columnHeight)
    # *****************************************************************************
    # OUTPUT VARIABLES
    # *****************************************************************************

    frameDetection = []
    frameDetectionMaxPoints = []
    frameDetectionMaxValues = []
    frameDetectionMinPoints = []
    frameDetectionMinValues = []

    # *****************************************************************************
    # STARTING - First Acquisition
    # *****************************************************************************

    streamer = FrameStreamer(videoFile, isRealTime=False)
    streamer.start()
    isGrabbed, frame = streamer.grabFrame()

    # *****************************************************************************
    # ROI Detection
    # *****************************************************************************
    countFrames = 0

    while(isGrabbed):
        isGreater,topLeft,maxVal,minPoint,minVal,roi=occrx.detector.detectRoi(frame, roiTemplate, threshold=0)
        frameDetectionMaxPoints.append(topLeft)
        frameDetectionMaxValues.append(maxVal)
        frameDetectionMinPoints.append(minPoint)
        frameDetectionMinValues.append(minVal)
        rectangle.setTopLeftCornerPoint(topLeft)
        isWithinCircle = occscripts.occscriptutils.isRectangleWithinCircle(rectangle,circle)
        frameDetection.append(isWithinCircle)

        isGrabbed, frame = streamer.grabFrame()
        countFrames = countFrames + 1
        if countFrames >= 1000:
            print("Part saving")
            detectionResults = DetectionResults(isWithinCircle=frameDetection,
                                                maxValues=frameDetectionMaxValues,
                                                maxLoc=frameDetectionMaxPoints,
                                                minValues=frameDetectionMinValues,
                                                minLoc=frameDetectionMinPoints)
            with open(testDirectory + "detectionresults.json", "w") as write_file:
                json.dump(detectionResults.toJSON(), write_file)
            countFrames = 0

    detectionResults = DetectionResults(isWithinCircle=frameDetection,
                                        maxValues=frameDetectionMaxValues,
                                        maxLoc=frameDetectionMaxPoints,
                                        minValues=frameDetectionMinValues,
                                        minLoc=frameDetectionMinPoints)
    with open(testDirectory + "detectionresults.json", "w") as write_file:
        json.dump(detectionResults.toJSON(), write_file)

    toc = time.clock()
    streamer.stop()
    print("Finished test after {} seconds".format(toc-tic))
    return

if __name__ == '__main__':
    import os
    import argparse

    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--input", type=str, required=True, help="base directory where the videos are stored")
    ap.add_argument("-ch", "--columnHeight", type=int, default=15, required=False,
                    help="column height used for correlation (default=15)")
    ap.add_argument("-r", "--nRoi", type=int, default=11, required=False,
                    help="number of roi rows (default = (2N+1)*packetRows)")
    ap.add_argument("-p", "--nPacket", type=int, default=5, required=False,
                    help="number of packet rows (default = 5)")
    ap.add_argument("-t", "--testNumber", type=int, default=5, required=False,
                    help="testNumber for detection tests.")

    args = vars(ap.parse_args())
    filename = str(args["input"])
    columnHeight = int(args["columnHeight"])
    numberOfRoiRows = int(args["nRoi"])
    numberOfPacketRows = int(args["nPacket"])
    testNumber = int(args["testNumber"])

    for file in os.listdir(filename):
        if not os.path.isfile(filename + "{}/test.ignore".format(file)):
            occSystemParams = occrx.occParams.OCCSystemParams()

            with open(filename + "" + file + "/occsystemparams.json", "r") as read_file:
                occSystemParams.initFromJSON(json.load(read_file))
            occTestParams = occrx.occParams.OCCTestParams(columnHeight=columnHeight,
                                                              numberOfFramesForCalibration=0,
                                                              numberOfRoiRows=numberOfRoiRows,
                                                              numberOfPacketRows=numberOfPacketRows,
                                                              roiThresholdLevel=0)
            baseFolder = filename + "" + file +"/"
            # *****************************************************************************
            # Directory VARIABLES
            # *****************************************************************************
            testDirectory = baseFolder + "detection/test{}/".format(testNumber)
            if not os.path.exists(testDirectory):
                os.makedirs(testDirectory)
                print('Init Test for: {}'.format(testDirectory))

                with open(testDirectory + '/occtestparams.json', "w") as write_file:
                    json.dump(occTestParams.toJSON(), write_file)
                with open(testDirectory + '/occsystemparams.json', "w") as write_file:
                    json.dump(occSystemParams.toJSON(), write_file)

                testPacketDetection(baseFolder,
                                    testDirectory,
                                    occSystemParams,
                                    occTestParams)
            else:
                print('Test --> {}, already performed'.format(testNumber))