import sys
sys.path.append("..")
import occrx
from occrx.framestreamer import FrameStreamer
import occrx.templates
import occrx.detector
import occrx.calibrator
import occrx.demodulator
import occrx.occParams
import os
import matplotlib.pyplot as plt
import numpy as np
import pickle
import cv2

def debugTest(baseDirectory,testNumber):
    print("Init debuging")
    testDirectory = baseDirectory+"test{}/".format(testNumber)
    errorDirectory = testDirectory+"error/"

    if not os.path.exists(errorDirectory):
        return
    print(len(os.listdir(errorDirectory))<1)
    if(len(os.listdir(errorDirectory))<1):
        return
    fileObjs = open(testDirectory + 'OCCSystemCalibration.obj', 'rb')

    calibrationMeasurements = pickle.load(fileObjs)
    calibrationFitting = pickle.load(fileObjs)
    invColorMatrix, threshold = pickle.load(fileObjs)
    roi = pickle.load(fileObjs)
    fileObjs.close()

    fileObjs = open(testDirectory + 'OCCTestParams.obj', 'rb')
    occTestParams = pickle.load(fileObjs)
    fileObjs.close()

    fileObjs = open(baseDirectory + 'OCCSystemParams.obj', 'rb')
    occSystemParams = pickle.load(fileObjs)
    fileObjs.close()

    syncTemplate = occrx.templates.generateSyncTemplate(occSystemParams.occRxParams.rowHeight)

    for filename in os.listdir(errorDirectory):
        print(errorDirectory+filename)
        streamer = FrameStreamer(errorDirectory+filename, isRealTime=False)
        streamerData = FrameStreamer(baseDirectory+"calibration.avi", isRealTime=False)

        # *****************************************************************************
        # Plot section
        # *****************************************************************************
        fig, ajustAx = plt.subplots(3, 1)

        ajustAx[0].title.set_text('Green channel interferences')
        ajustAx[0].plot(calibrationMeasurements[:, 0,1], c='#a7adba')
        ajustAx[0].plot(calibrationMeasurements[:, 1,1], c='#293336')
        ajustAx[0].plot(calibrationMeasurements[:, 2,1], c='#a7adba')

        ajustAx[0].plot(calibrationFitting[:, 0,1], c='r')
        ajustAx[0].plot(calibrationFitting[:, 1,1], c='g')
        ajustAx[0].plot(calibrationFitting[:, 2,1], c='b')

        ajustAx[1].title.set_text('Red channel interferences')
        ajustAx[1].plot(calibrationMeasurements[:, 0,0], c='#293336')
        ajustAx[1].plot(calibrationMeasurements[:, 1,0], c='#a7adba')
        ajustAx[1].plot(calibrationMeasurements[:, 2,0], c='#a7adba')

        ajustAx[1].plot(calibrationFitting[:, 0,0], c='r')
        ajustAx[1].plot(calibrationFitting[:, 1,0], c='g')
        ajustAx[1].plot(calibrationFitting[:, 2,0], c='b')

        ajustAx[2].title.set_text('Blue channel interferences')
        ajustAx[2].plot(calibrationMeasurements[:, 0,2], c='#a7adba')
        ajustAx[2].plot(calibrationMeasurements[:, 1,2], c='#a7adba')
        ajustAx[2].plot(calibrationMeasurements[:, 2,2], c='#293336')

        ajustAx[2].plot(calibrationFitting[:, 0,2], c='r')
        ajustAx[2].plot(calibrationFitting[:, 1,2], c='g')
        ajustAx[2].plot(calibrationFitting[:, 2,2], c='b')

        plt.show()

        # *****************************************************************************
        # Plot section
        # *****************************************************************************

        isGrabbed, frame = streamer.grabFrame()
        isGrabbedData, frameData = streamerData.grabFrame()
        # Mask ROI#
        frame = frame **(1/1.8)
        roiFrame = frame[roi[0][1]:roi[1][1], roi[0][0]:roi[1][0], :].copy()
        roiFrameData = frameData[roi[0][1]:roi[1][1], roi[0][0]:roi[1][0], :].copy()
        roiFoundFrame = frame.copy()
        cv2.rectangle(roiFoundFrame, tuple(roi[0]), tuple(roi[1]), (1.0, 1.0, 1.0), 2)
        
        roiFoundFrameData = frameData.copy()
        cv2.rectangle(roiFoundFrameData, tuple(roi[0]), tuple(roi[1]), (1.0, 1.0, 1.0), 2)

        plt.imshow(roiFoundFrame)
        plt.show()
        plt.imshow(roiFrame)
        plt.show()
        plt.imshow(roiFoundFrameData)
        plt.show()
        roiFrame = np.mean(roiFrame, axis=1)
        # Enhance
        colorAdaptedSignalFiltered = occrx.calibrator.enhancing(roiFrame, invColorMatrix)

        # *****************************************************************************
        # Plot section
        # *****************************************************************************
        fig, colorAdaptedAx = plt.subplots(3, 2)
        colorAdaptedAx[0, 0].title.set_text('Raw signal')
        colorAdaptedAx[0, 0].plot(roiFrame[:, 0], c='r')
        colorAdaptedAx[0, 1].title.set_text('Color adapted signal')
        colorAdaptedAx[0, 1].plot(colorAdaptedSignalFiltered[:, 0], c='r')
        colorAdaptedAx[0, 1].plot(threshold[:, 0]/2, c='r')
        colorAdaptedAx[1, 0].plot(roiFrame[:, 1], c='g')
        colorAdaptedAx[1, 1].plot(colorAdaptedSignalFiltered[:, 1], c='g')
        colorAdaptedAx[1, 1].plot(threshold[:, 1]/2, c='g')
        colorAdaptedAx[2, 0].plot(roiFrame[:, 2], c='b')
        colorAdaptedAx[2, 1].plot(colorAdaptedSignalFiltered[:, 2], c='b')
        colorAdaptedAx[2, 1].plot(threshold[:, 2]/2, c='b')

        roiFrame = np.repeat(roiFrame, 30, axis=1)

        colorAdaptedSignalFilteredPlot = colorAdaptedSignalFiltered.copy()
        colorAdaptedSignalFilteredPlot = np.repeat(colorAdaptedSignalFilteredPlot, 30, axis=1)
        fig, colorAdaptedPlotAx = plt.subplots(1, 2)
        colorAdaptedPlotAx[0].title.set_text('Raw signal')
        colorAdaptedPlotAx[0].imshow(roiFrame)
        colorAdaptedPlotAx[1].title.set_text('Color adapted signal filtered')
        colorAdaptedPlotAx[1].imshow(colorAdaptedSignalFilteredPlot)

        plt.show()
        # *****************************************************************************
        # Plot section
        # *****************************************************************************

        # Binarize
        binarizeSignal = occrx.demodulator.binarize(colorAdaptedSignalFiltered, threshold)
        data = occrx.demodulator.decode(binarizeSignal, syncTemplate)
        data = (data.flatten()).astype(np.int)
        # print(data)
        isValid = occrx.demodulator.checkCRC(''.join(map(str, (data[0:10]))), '0101')
        if not isValid:
            print("ERROR")
            # Save error frame for later processing
            frameO = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
            cv2.imwrite(errorDirectory + "error{}.jpg".format(int(errorCounts)), (frameO * 255).astype(np.uint8))
            errorCounts = errorCounts + 1;
            if (errorCounts >= 10):
                print('Too many errors')
                return
        else:
            # print("VALID")
            validCounts = validCounts + 1;

    print("Finished debug")

if __name__ == '__main__':
    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument("-f", "--filename",type=str, required=True, help="base directory where the videos are stored")
    ap.add_argument("-t", "--testNumber",type=int, default=0, required=True, help="test debug number")
    
    args = vars(ap.parse_args())
    filename = str(args["filename"])
    testNumber = int(args["testNumber"])

    print("filename {}, type {}".format(filename, type(filename)))
    print("testNumber {}, type {}".format(testNumber, type(testNumber)))
    
    debugTest(filename, testNumber)