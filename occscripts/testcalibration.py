import sys
sys.path.append("..")
import cv2
import numpy as np
from occrx.framestreamer import FrameStreamer
import occrx.occParams
import occrx.templates
import occrx.detector
import occrx.calibrator
import occscripts.occscriptutils
from occscripts.occscriptutils import Circle, Rectangle
import json
from jsonserializable import JSONSerializable
import random
import time
from occscripts.testdetection import DetectionResults

class CalibrationResults(JSONSerializable):
    def __init__(self, roiPoint = None,
                 roiValue = None,
                 realValues = None,
                 measuredValues = None,
                 predictedValues = None,
                 enhancedThreshold = None,
                 inverseMatrix = None,
                 numberOfCalibrations = None):

        self.roiPoint = roiPoint
        self.roiValue = roiValue
        self.realValues = realValues
        self.measuredValues = measuredValues
        self.predictedValues = predictedValues
        self.enhancedThreshold = enhancedThreshold
        self.inverseMatrix = inverseMatrix
        self.numberOfCalibrations = numberOfCalibrations

    def toJSON(self):
        data = {
            "roi": json.dumps(self.roiPoint.tolist(), separators=(',', ':'), sort_keys=True, indent=4),
            "roiValue": float(self.roiValue),
            "realValues": json.dumps(self.realValues.tolist(), separators=(',', ':'), sort_keys=True, indent=4),
            "measuredValues": json.dumps(self.measuredValues.tolist(), separators=(',', ':'), sort_keys=True, indent=4),
            "predictedValues": json.dumps(self.predictedValues.tolist(), separators=(',', ':'), sort_keys=True, indent=4),
            "enhancedThreshold": json.dumps(self.enhancedThreshold.tolist(), separators=(',', ':'), sort_keys=True, indent=4),
            "inverseMatrix": json.dumps(self.inverseMatrix.tolist(), separators=(',', ':'), sort_keys=True, indent=4),
            "numberOfCalibrations": int(self.numberOfCalibrations)
        }
        return data

    def initFromJSON(self, jsonData):
        self.roiPoint = np.array(json.loads(jsonData["roi"]))
        self.roiValue = float(jsonData["roiValue"])
        self.realValues = np.array(json.loads(jsonData["realValues"]))
        self.measuredValues = np.array(json.loads(jsonData["measuredValues"]))
        self.predictedValues = np.array(json.loads(jsonData["predictedValues"]))
        self.enhancedThreshold = np.array(json.loads(jsonData["enhancedThreshold"]))
        self.inverseMatrix = np.array(json.loads(jsonData["inverseMatrix"]))
        self.numberOfCalibrations = int(jsonData["numberOfCalibrations"])

def testCalibration(baseFolder, occSystemParams, occTestParams, detectionResults,calibrations):
    tic = time.clock()

    genuineOcurrences = np.where(np.array(detectionResults.maxValues) >= occTestParams.roiThresholdLevel)[0]
    print("Entering")
    if(len(genuineOcurrences)>0):
        print("Entering 2")
        randomIndex = random.randint(0, len(genuineOcurrences) - 1)
        genuineOcurrence = genuineOcurrences[randomIndex]
        while (genuineOcurrence > (len(detectionResults.isWithinCircle) - 171)):
            genuineOcurrence = genuineOcurrence - 1
            if (genuineOcurrence <= 0):
                print("Not found sample that goes over that threshold")
                return False
        print("Genuine ocurrence: {}".format(genuineOcurrence))
        for calibration in calibrations:
            occTestParams.numberOfFramesForCalibration=calibration
            baseTestDirectory = baseFolder+"calibration/cal{}_th{}/".format(calibration,occTestParams.roiThresholdLevel)
            if not os.path.exists(baseTestDirectory):
                os.makedirs(baseTestDirectory)

            video = baseFolder+'/calibration.avi'

            circle = Circle()
            with open(baseFolder + "circle.json", "r") as read_file:
                circle.initFromJSON(json.load(read_file))

            # *****************************************************************************
            # GENERATING VARIABLES
            # *****************************************************************************

            maxLoc = detectionResults.maxLoc[genuineOcurrence]
            maxValue = detectionResults.maxValues[genuineOcurrence]
            top_left = maxLoc
            w = occTestParams.columnHeight
            h = occTestParams.numberOfRoiRows*occSystemParams.occRxParams.rowHeight
            bottom_right = (top_left[0] + w, top_left[1] + h)
            roi= np.array((top_left, bottom_right))

            roiPattern = occrx.templates.generateFinalPattern(occSystemParams.occTxParams.patterns.roiPattern,
                                                              occTestParams.numberOfRoiRows)
            packetPattern = occrx.templates.generateFinalPattern(occSystemParams.occTxParams.patterns.roiPattern,
                                                                 occTestParams.numberOfPacketRows)
            calibrationPattern = occrx.templates.generateFinalPattern(occSystemParams.occTxParams.patterns.roiPattern,
                                                                 occTestParams.numberOfPacketRows*2)
            # ============================================================================
            # Correlation templates
            # ============================================================================

            roiTemplate = occrx.templates.generateTemplate(pattern=roiPattern,
                                                           rowHeight=occSystemParams.occRxParams.rowHeight,
                                                           columnHeight=occTestParams.columnHeight)

            packetTemplate = occrx.templates.generateTemplate(pattern=packetPattern,
                                                              rowHeight=occSystemParams.occRxParams.rowHeight,
                                                              columnHeight=occTestParams.columnHeight)

            # ============================================================================
            # Calibration variables
            # ============================================================================
            roiHeight = roiTemplate.shape[0]
            calibrationMeasurements = np.zeros((roiHeight, 3, 3))
            calibrationFitting = np.zeros((roiHeight,3,3))
            invColorMatrix = 0
            threshold = 0
            enhancedThreshold = np.zeros((roiHeight, 3))

            # *****************************************************************************
            # *****************************************************************************

            streamer = FrameStreamer(video, isRealTime=False)
            # *****************************************************************************
            # Grab frame until next Genuine Sample
            # *****************************************************************************

            streamer.setFrameStart(genuineOcurrence)
            streamer.start()
            isGrabbed,frame = streamer.grabFrame()
            status,top_left,max_val,min_loc,min_val,det_roi = occrx.detector.detectRoi(frame, roiTemplate, threshold=occTestParams.roiThresholdLevel)

            print(m)
            print(roi==det_roi)
            print(roi)
            print(det_roi)

            height = occSystemParams.occRxParams.rowHeight * occTestParams.numberOfRoiRows
            width = occTestParams.columnHeight

            rectangle = Rectangle(topLeftCornerPoint=(0, 0), width=width, height=height)
            rectangle.setTopLeftCornerPoint(top_left)
            isWithingCircle = occscripts.occscriptutils.isRectangleWithinCircle(rectangle, circle)
            print("Is template within circle: {}".format(isWithingCircle))

            testStep = 0
            if(isWithingCircle):
                testDirectory = baseTestDirectory + "true{}/".format(testStep)
            else:
                testDirectory = baseTestDirectory + "false{}/".format(testStep)

            while os.path.exists(testDirectory):
                testStep = testStep + 1
                if(isWithingCircle):
                    testDirectory = baseTestDirectory + "true{}/".format(testStep)
                else:
                    testDirectory = baseTestDirectory + "false{}/".format(testStep)
            os.makedirs(testDirectory)

            print('Init Calibration Test for: {}'.format(testDirectory))

            with open(testDirectory + '/occtestparams.json', "w") as write_file:
                json.dump(occTestParams.toJSON(), write_file)
            with open(testDirectory + '/occsystemparams.json', "w") as write_file:
                json.dump(occSystemParams.toJSON(), write_file)
            with open(testDirectory + '/detectionresults.json', "w") as write_file:
                json.dump(detectionResults.toJSON(), write_file)
            # *****************************************************************************
            # Calibration
            # *****************************************************************************

            roiFrame = frame[roi[0][1]:roi[1][1], roi[0][0]:roi[1][0], :].copy()
            calibrationMeasurements= occrx.calibrator.resetCalibration(roiPattern,
                                                                       roiFrame,
                                                                       occSystemParams.occRxParams.rowHeight,
                                                                       calibrationMeasurements)

            for i in range(0, occTestParams.numberOfFramesForCalibration):
                isGrabbed, frame = streamer.grabFrame()
                roiFrame = frame[roi[0][1]:roi[1][1], roi[0][0]:roi[1][0], :].copy()

                calibrationMeasurements = occrx.calibrator.meassuring(calibrationPattern,
                                                                      roiFrame,
                                                                      occSystemParams.occRxParams.rowHeight,
                                                                      packetTemplate,
                                                                      calibrationMeasurements)

            calibrationFitting[:, :, 0] = occrx.calibrator.fitting(calibrationMeasurements[:, :, 0])
            calibrationFitting[:, :, 1] = occrx.calibrator.fitting(calibrationMeasurements[:, :, 1])
            calibrationFitting[:, :, 2] = occrx.calibrator.fitting(calibrationMeasurements[:, :, 2])

            invColorMatrix, threshold = occrx.calibrator.calibrateChannels(calibrationFitting)

            enhancedThreshold[:, 0] = occrx.calibrator.computeThreshold(invColorMatrix, calibrationFitting[:, :, 0])[:, 0]
            enhancedThreshold[:, 1] = occrx.calibrator.computeThreshold(invColorMatrix, calibrationFitting[:, :, 1])[:, 1]
            enhancedThreshold[:, 2] = occrx.calibrator.computeThreshold(invColorMatrix, calibrationFitting[:, :, 2])[:, 2]
            redImg = cv2.imread(baseFolder+"red.jpg")
            b, g, r = cv2.split(redImg)
            redImg = (cv2.merge([r, g, b])/255.0)
            redImg = redImg**1.8
            greenImg = cv2.imread(baseFolder+"green.jpg")
            b, g, r = cv2.split(greenImg)
            greenImg = (cv2.merge([r, g, b])/255.0)
            greenImg = greenImg**1.8
            blueImg = cv2.imread(baseFolder+"blue.jpg")
            b, g, r = cv2.split(blueImg)
            blueImg = (cv2.merge([r, g, b])/255.0)
            blueImg = blueImg**1.8
            realValues = np.empty(calibrationFitting.shape).astype(np.float)
            realValues[:,:,0] = np.mean(redImg[roi[0][1]:roi[1][1], roi[0][0]:roi[1][0], :].copy(),axis=1)
            realValues[:,:,1] = np.mean(greenImg[roi[0][1]:roi[1][1], roi[0][0]:roi[1][0], :].copy(),axis=1)
            realValues[:,:,2] = np.mean(blueImg[roi[0][1]:roi[1][1], roi[0][0]:roi[1][0], :].copy(),axis=1)

            calibrationResults = CalibrationResults(roiPoint=roi,
                                                    roiValue=maxValue,
                                                    realValues=realValues,
                                                    measuredValues=calibrationMeasurements,
                                                    predictedValues=calibrationFitting,
                                                    enhancedThreshold=enhancedThreshold,
                                                    inverseMatrix=invColorMatrix,
                                                    numberOfCalibrations=occTestParams.numberOfFramesForCalibration)
            with open(testDirectory + '/calibrationresults.json', "w") as write_file:
                json.dump(calibrationResults.toJSON(), write_file)
            toc = time.clock()
            streamer.stop()
            print("Finished test after {} seconds".format(toc-tic))

if __name__ == '__main__':
    import os
    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--input", type=str, required=True, help="base directory where the videos are stored")
    ap.add_argument("-c", "--calibrations", type=str, required=True,
                    help="list of number of calibrations separated by commas 1,2,3")
    ap.add_argument("-d", "--detectionTestNumber", type=int, required=True,
                    help="previous detection test number")
    ap.add_argument("-r", "--repetitions", type=int, required=True,
                    help="Number of test repetitions")
    ap.add_argument("-t", "--threshold", type=float, default=0.80, required=False,
                    help="threshold level 0..1 (default=0.80)")

    args = vars(ap.parse_args())
    filename = str(args["input"])
    calibrations = list(map(int, (args["calibrations"].split(","))))
    detectionTestNumber = int(args["detectionTestNumber"])
    repetitions = int(args["repetitions"])
    roiThresholdLevel = float(args["threshold"])

    for file in os.listdir(filename):
        if not os.path.isfile(filename + "{}/test.ignore".format(file)):
            for m in range(0,repetitions):
                occSystemParams = occrx.occParams.OCCSystemParams()
                with open(filename + "" + file + "/occsystemparams.json", "r") as read_file:
                    occSystemParams.initFromJSON(json.load(read_file))

                occTestParams = occrx.occParams.OCCTestParams()
                with open(filename + "" + file + "/detection/test{}/occtestparams.json".format(detectionTestNumber), "r") as read_file:
                    occTestParams.initFromJSON(json.load(read_file))
                occTestParams.roiThresholdLevel=roiThresholdLevel

                detectionResults = DetectionResults()
                with open(filename + "" + file + "/detection/test{}/detectionresults.json".format(detectionTestNumber), "r") as read_file:
                    detectionResults.initFromJSON(json.load(read_file))

                testCalibration(filename + "" + file +"/",
                                                 occSystemParams,
                                                 occTestParams,
                                                 detectionResults,
                                calibrations)