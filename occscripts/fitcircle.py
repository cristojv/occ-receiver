import cv2
import occscriptutils
import json

if __name__ == '__main__':
    import os
    import argparse

    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--inputFolder", type=str, required=True, help="folder input files")

    args = vars(ap.parse_args())
    filename = str(args["inputFolder"])

    for file in os.listdir(filename):
        if not os.path.isfile(filename+"{}/test.ignore".format(file)):

            cap = cv2.VideoCapture(filename+'{}/calibration.avi'.format(file))
            s, img = cap.read()
            width, height, channels=img.shape
            circle = occscriptutils.Circle((height//2,width//2),10)
            while(True):
                copy = img.copy()
                cv2.circle(copy, (circle.centroid[0],circle.centroid[1]), circle.radius, (255, 255, 255), 1)
                cv2.imshow("Fitting radius", copy)
                key = cv2.waitKey(0)
                if key == ord('q'):
                    print("Finishing")
                    break
                elif key == ord('s'):
                    print("Saving")
                    print('Circle radius:{}\nCircle points: x->{}, y->{}'.format(circle.radius,circle.centroid[0],circle.centroid[1]))

                    with open(filename+file+'/circle.json', "w") as write_file:
                        json.dump(circle.toJSON(), write_file)
                    break
                elif key == 81:
                    print("Moving Left")
                    circle.moveX(1,False)
                elif key == 82:
                    print("Moving Up")
                    circle.moveY(1,True)
                elif key == 83:
                    print("Moving Right")
                    circle.moveX(1,True)
                elif key == 84:
                    print("Moving Down")
                    circle.moveY(1,False)
                elif key == 43:
                    print("Incrementing radius")
                    circle.incrementRadius(1,True)
                elif key == 45:
                    print("Incrementing radius")
                    circle.incrementRadius(1,False)
                else:
                    print(key)
            cv2.destroyAllWindows()
            cap.release()