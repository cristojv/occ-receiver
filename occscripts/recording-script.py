import sys
sys.path.append("..")
import serial
import cv2
import time
import occrx.occParams
import json

##################################################################
# File Parameters
##################################################################

baseFilename = "./"
baseVideo = "videos/"
postVideo = ""
postVideoData = "data"
postVideoFps = "fps"
videoFormat = ".avi"
fourcc = cv2.VideoWriter_fourcc(*'MJPG')

# frequencies = [6480,5400,4050,3600,3240, 2160, 1620]
# rowHeights = [5,6,8,9,10,15,20]

##################################################################
# Camera Parameters
##################################################################
frameWidth = 1920
frameHeight = 1080
fps = 30
gain = 75
wbtm = 0
wbt = 6500
expm = 1
exp = 3
fm = 0
focus = 0
##################################################################
# Utils Functions
##################################################################

def printCameraParameters():
    print("##################################################################\n"
          "Parametros de la camara\n"
          "##################################################################\n"
          "Frame sizes: {}x{}\n"
          "FPS: {}\n"
          "Ganancia: {}\n"
          "Modo WB: {}\n"
          "WB Gains: {}\n"
          "Modo Exp: {}\n"
          "Exp: {}\n".format(cap.get(cv2.CAP_PROP_FRAME_WIDTH),
                             cap.get(cv2.CAP_PROP_FRAME_HEIGHT),
                             cap.get(cv2.CAP_PROP_FPS),
                             cap.get(cv2.CAP_PROP_GAIN),
                             wbtm,
                             wbt,
                             expm,
                             cap.get(cv2.CAP_PROP_EXPOSURE))
          )
    time.sleep(1)

if __name__ == '__main__':
    import os
    import argparse

    ##################################################################
    # Transmitter Parameters
    ##################################################################

    portSerial = "/dev/ttyACM0"
    baudRate = 115200
    timeoutSerial = 1

    ap = argparse.ArgumentParser()
    ap.add_argument("-o", "--output", type=str, required=True, help="output folder to store the videos.")
    ap.add_argument("-d", "--distance", type=int, default=15, required=True,
                    help="distance in cm")
    ap.add_argument("-f", "--frequencies", type=str, required=True,
                    help="list of frequencies separated by commas 1000,2000,3000 in Hz")
    ap.add_argument("-r", "--resolution",default='1920x1080', type=str, required=False,
                    help="resolution in format 1920x1080 (default=1920x1080)")
    ap.add_argument("-fps", "--fps",default='30.0', type=float, required=False,
                    help="framerate of the camera (default=30.0)")
    ap.add_argument("-g", "--gain", type=int, default=75, required=False,
                    help="gain of the camera (default=75)")
    ap.add_argument("-wb", "--whitebalance", type=int, default=6500, required=False,
                    help="whitebalance of the camera (default=6500)")
    ap.add_argument("-e", "--exposure", type=int, default=3, required=False,
                    help="exposure of the camera in hundreds of microseconds (default=3//300us)")
    ap.add_argument("-fo", "--focus", type=int, default=0, required=False,
                    help="focus of the camera (default=0)")
    ap.add_argument("-m", "--mode", type=int, default=1, required=False,
                    help="mode of capture. Capture (0) calibration (1) calibration and data (default=1)")
    ap.add_argument("-c", "--cameradevice", type=int, default=0, required=False,
                    help="camera device number (default=0)")
    ap.add_argument("-ct", "--calibrationtime", type=int, required=True,
                    help="calibration time in seconds")
    ap.add_argument("-dt", "--datatime", type=int, required=True,
                    help="data time in seconds")

    args = vars(ap.parse_args())
    filename = str(args["output"])
    frequencies = list(map(int, (args["frequencies"].split(","))))

    ##################################################################
    # Camera Parameters
    ##################################################################
    resolution = list(map(int, (args["resolution"].split("x"))))
    frameWidth = int(resolution[0])
    frameHeight = int(resolution[1])
    fps = int(args["fps"])
    gain = int(args["gain"])
    wbtm = 0
    wbt = int(args["whitebalance"])
    expm = 1
    exp = int(args["exposure"])
    fm = 0
    focus = int(args["focus"])
    mode = int(args["mode"])
    captureDuration = int(args["calibrationtime"])
    captureDataDuration = int(args["datatime"])
    cameraDevice = int(args["cameradevice"])


    ##################################################################
    # Channel Parameters
    ##################################################################
    distance = int(args["distance"])

    print("Introduced parameters")
    print("output {}, type {}".format(filename, type(filename)))
    print("distance {}, type {}".format(distance, type(distance)))
    print("frequencies {}, type {}".format(frequencies, type(frequencies)))
    print("resolution {}x{}, type {}, type{}".format(frameWidth, frameHeight, type(frameWidth),type(frameHeight)))
    print("fps {}, type {}".format(fps, type(fps)))
    print("gain {}, type {}".format(gain, type(gain)))
    print("white balance {}, type {}".format(wbt, type(wbt)))
    print("exposure {}, type {}".format(exp, type(exp)))
    print("focus {}, type {}".format(focus, type(focus)))
    print("mode {}, type {}".format(mode, type(mode)))
    print("calibration time {}, type {}".format(captureDuration, type(captureDuration)))
    print("camera device number ({}), type {}".format(cameraDevice, type(cameraDevice)))

    ard = serial.Serial(portSerial, baudRate, timeout=timeoutSerial)

    cap = cv2.VideoCapture(cameraDevice)
    cap.set(cv2.CAP_PROP_FOURCC,fourcc)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH,frameWidth)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT,frameHeight)
    bSuccess = cap.set(cv2.CAP_PROP_FPS, fps)
    os.system("v4l2-ctl -d /dev/video{} --set-ctrl=gain={}".format(cameraDevice,gain))
    time.sleep(1)
    os.system("v4l2-ctl -d /dev/video{} --set-ctrl=white_balance_temperature_auto={}".format(cameraDevice,wbtm))
    time.sleep(1)
    os.system("v4l2-ctl -d /dev/video{} --set-ctrl=white_balance_temperature={}".format(cameraDevice,wbt))
    time.sleep(1)
    os.system("v4l2-ctl -d /dev/video{} --set-ctrl=exposure_auto={}".format(cameraDevice,expm))
    time.sleep(1)
    os.system("v4l2-ctl -d /dev/video{} --set-ctrl=exposure_absolute={}".format(cameraDevice,exp))
    time.sleep(1)
    os.system("v4l2-ctl -d /dev/video{} --set-ctrl=focus_auto={}".format(cameraDevice,0))
    time.sleep(1)
    os.system("v4l2-ctl -d /dev/video{} --set-ctrl=focus_absolute={}".format(cameraDevice,focus))
    time.sleep(1)

    '''
    https://docs.opencv.org/3.1.0/d8/dfe/classcv_1_1VideoCapture.html
    '''

    for i in range(0,len(frequencies)):

        print("##################################################################\n"
              "COMENZANDO TEST Para Frecuencia: {}\n"
              "##################################################################\n".format(frequencies[i]))

        heightResolution = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
        widthResolution = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
        fpsCamera = cap.get(cv2.CAP_PROP_FPS)
        gainCamera = cap.get(cv2.CAP_PROP_GAIN)
        exposureCamera = cap.get(cv2.CAP_PROP_EXPOSURE)
        videoDirectory = filename+"f{}_s{}x{}_fps{}_g{}_wb{}_exp{}_distance{}cm/".format(frequencies[i],
                                                                                    widthResolution, heightResolution,
                                                                                    fpsCamera,
                                                                                    gainCamera,
                                                                                    wbt,
                                                                                    exposureCamera,
                                                                                    distance)
        if not os.path.exists(videoDirectory):
            os.makedirs(videoDirectory)

        #Save three pictures for the red, the green and the blue channel.
        imageFile = videoDirectory + "img.jpg"

        ard.write('er'.format(0).encode('utf-8'))
        time.sleep(0.5)
        for j in range(0, 40):
            ret, frame = cap.read()
        ret, frame = cap.read()
        cv2.imwrite(videoDirectory+"red.jpg", frame)

        ard.write('g'.format(0).encode('utf-8'))
        time.sleep(0.5)
        for j in range(0, 40):
            ret, frame = cap.read()
        ret, frame = cap.read()
        cv2.imwrite(videoDirectory+"green.jpg", frame)

        ard.write('b'.format(0).encode('utf-8'))
        time.sleep(0.5)
        for j in range(0, 40):
            ret, frame = cap.read()
        ret, frame = cap.read()
        cv2.imwrite(videoDirectory+"blue.jpg", frame)

        printCameraParameters()

        #Save calibration video.
        ard.write('ecf{} xs'.format(frequencies[i]).encode('utf-8'))
        time.sleep(1)

        video = cv2.VideoWriter(videoDirectory+"calibration.avi",fourcc, int(fpsCamera), (int(widthResolution),int(heightResolution)))

        for j in range (0,40):
            ret, frame = cap.read()

        start_time = time.time()
        while (int(time.time() - start_time) < captureDuration):
            ret, frame = cap.read()
            if ret == True:
                video.write(frame)
            else:
                print("error")
                break
        video.release()

        videoData = 0
        time.sleep(1)


        videoData = cv2.VideoWriter(videoDirectory + "/data.avi",fourcc, int(fpsCamera), (int(widthResolution),int(heightResolution)))
        for j in range(0, 30):
            ret, frame = cap.read()

        time.sleep(0.2)
        ard.write('d'.encode('utf-8'))
        start_time = time.time()
        while (int(time.time() - start_time) < captureDataDuration):
            ret, frame = cap.read()
            if ret == True:
                videoData.write(frame)
            else:
                print("error")
                break
        videoData.release()
        # ard.write('d'.encode('utf-8'))
        time.sleep(2)

        ##################################################################
        # Saving Parameters
        ##################################################################
        occTxParams = occrx.occParams.OCCTxParams(txFrequency=frequencies[i],
                                                  patterns=occrx.occParams.OCCTxParams.Patterns([1,0,1,2,3],[1,1,3,1,3]))
        occRxParams = occrx.occParams.OCCRxParams(resolution=(widthResolution,heightResolution),
                                                  gain = gainCamera,
                                                  whitebalance= wbt,
                                                  exposure=exposureCamera,
                                                  samplingFrequency=fpsCamera,
                                                  rowHeight=int(heightResolution/(frequencies[i]/fpsCamera)))
        occChannelParams = occrx.occParams.OCCChannelParams(distance=distance)

        occSystemParams = occrx.occParams.OCCSystemParams(occTxParams,occChannelParams,occRxParams)

        with open(videoDirectory + '/occsystemparams.json', "w") as write_file:
            json.dump(occSystemParams.toJSON(), write_file)

        # fileObjs = open(videoDirectory+'/OCCSystemParams.obj', 'wb')
        # pickle.dump(occSystemParams, fileObjs)
        # fileObjs.close()

    print("Recording finalizado")